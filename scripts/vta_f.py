import samri.plotting.maps as maps
from samri.fetch.local import roi_from_atlaslabel

stat_map = "data/vta_f.nii.gz"
template = "/usr/share/mouse-brain-atlases/dsurqec_40micron_masked.nii"

maps.stat3D(stat_map,
	scale=0.3,
	template=template,
	show_plot=False,
	threshold=3,
	)
