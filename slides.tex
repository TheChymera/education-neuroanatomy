\input{slides/header.tex}
\input{common_header.tex}

\title{Education: Neuroanatomy of Selected Monoaminergic Systems}
\subtitle{\href{https://bitbucket.com/TheChymera/education-neuroanatomy}{\small\texttt{[ bitbucket.com/TheChymera/education-neuroanatomy ]}}}
\author[Horea Christian]{Horea Christian\\\href{http://chymera.eu}{\small\texttt{[ chymera.eu ]}}}
\institute{Institute for Biomedical Engineering, ETH and University of Zürich}
\begin{document}
	\begin{frame}
		\titlepage
	\end{frame}
	\section{Brain Stem}
		\subsection{The Main Location of Monoamine Production}
			\begin{frame}{The Brain Stem}
				\begin{itemize}
					\item Evolutionarily conserved: structure, neurotransmitter systems
					\item Constituent structures perform unconscious functions
					\item Network structure and dynamics arguably simpler than in cortex
				\end{itemize}
				\begin{figure}[htp] \centering{
					\includegraphics[width=.52\textwidth]{img/brainstem.pdf}}
					\caption{Schematic representation of brain subsections (brainstem highlighted in colour)}
				\end{figure}
			\end{frame}
			\begin{frame}{Monoamines --- Evolutionarily Conservation}
				\vspace{.4em}
				\begin{minipage}{.28\textwidth}
					\begin{figure}[htp] \centering{
						\includegraphics[width=.8\textwidth]{img/dopamine_species.png}}
						\vspace{-.6em}
						\caption{
							Dopamine Systems \cite{Yamamoto2011}
							}
					\end{figure}
				\end{minipage}
				\begin{minipage}{.35\textwidth}
					\begin{figure}[htp] \centering{
						\includegraphics[width=.9\textwidth]{img/5ht_crayfish.png}}
						\caption{“Social Dominance and Serotonin Receptor Genes in Crayfish” \cite{Edwards2006}}
					\end{figure}
				\end{minipage}
				\begin{minipage}{.35\textwidth}
					\begin{figure}[htp] \centering{
						\includegraphics[width=.9\textwidth]{img/5ht_monkeys.png}}
						\caption{“Serotonergic mechanisms promote dominance acquisition in adult male vervet monkeys” \cite{Raleigh1991}}
					\end{figure}
				\end{minipage}
			\end{frame}
			\begin{frame}{Monoamines --- Chemistry}
				\begin{itemize}
					\item Histamine
					\item Catecholamines (from Tyrosine):
						\begin{itemize}
							\item Adrenaline
							\item \textbf{Dopamine (DA)}
							\item \textbf{Noradrenaline (NA)}
						\end{itemize}
					\item Indolamines (from Tryptophan):
						\begin{itemize}
							\item \textbf{Serotonin (5HT)}
							\item Melatonin
						\end{itemize}
				\end{itemize}
				\begin{figure}[htp] \centering{
					\includegraphics[scale=0.2]{img/pea.png}}
					\caption{Phenethylamine - the monoamine backbone structure}
				\end{figure}
			\end{frame}
			\begin{frame}{Selected Systems}
				\begin{figure}[htp]
					\centering
					\captionsetup{justification=centering}
					\includegraphics[width=.68\textwidth]{img/all}
					\caption{
						Serotonergic (green), Dopaminergic(orange), and Noradrenergic (blue) nuclei of the brainstem\\
						adapted from \cite{Paivi}
						}
				\end{figure}
			\end{frame}
	\section{Dorsal Raphe (DR)}
		\subsection{\textcolor{mygreen}{Serotonergic Forebrain Innervation}}
			\begin{frame}{5HT Formations: $\sim$7 Raphe Nuclei}
				\begin{itemize}
					\item Fused bilateral aggregations along midline - “Raphe” $\approx$ “seam” [Greek]
					\item Rostral Raphe Nulei have ascending projections \cite{Oegren2008}
					\item Bulk of 5HT forebrain projections stem from DR and Median Raphe (MR) Nucleus
				\end{itemize}
				\begin{figure}[htp] \centering{
					\includegraphics[scale=0.38]{img/rnn.png}}
					\vspace{-.5em}
					\caption{Highlighted serotonergic brainstem nuclei. Adapted from \cite{Siegel1999}}
				\end{figure}
			\end{frame}
			\begin{frame}{Appearance, Location, Structure}
				\begin{minipage}{.45\textwidth}
					The Dorsal Raphe proper:
					\begin{itemize}
						\item In dorsomedial midbrain tegmentum
						\begin{itemize}
							\item underneath aqueduct
						\end{itemize}
						\item Is not the \textcolor{lg}{Red Nucleus}!
					\end{itemize}
				\end{minipage}
				\begin{minipage}{.51\textwidth}
				\vspace{-2em}
				\py{pytex_fig(
					'scripts/dr.py',
					conf='slides/map_nocbar.conf',
					caption='
						Volumetric plot of the Dorsal Raphe.
						\\vspace{-1em}
						',
					label='dr',
					options_pre='\\centering\n',
					environment='figure',
					)}
				\end{minipage}\hfill
			\end{frame}
			\begin{frame}{Projections}
				\begin{itemize}
					\item DR projects to the majority of forebrain areas.
				\end{itemize}
				\begin{figure}[htp] \centering{
					\includegraphics[width=.6\textwidth]{img/dr}}
					\caption{Regions from \cite{Steinbusch1981,Lesch2012,PollakDorocic2014}}
				\end{figure}
			\end{frame}
			\begin{frame}{Highly Stable Functional Effects}
				\begin{minipage}{.45\textwidth}
					\begin{figure}[htp] \centering{
						\includegraphics[width=.85\textwidth]{img/dr_f_grandjean.png}}
						\caption{From \cite{Grandjean2019}}
					\end{figure}
				\end{minipage}
				\begin{minipage}{.45\textwidth}
					\vspace{-1em}
					\py{pytex_fig(
						'scripts/dr_f.py',
						conf='slides/map.conf',
						options_pre_caption='\\vspace{-2.5em}',
						caption='
							From \\cite{drlfom}
							',
						label='drf',
						options_pre='\\centering\n',
						environment='figure',
						)}
				\end{minipage}\hfill
			\end{frame}
			\begin{frame}{Projections -- more on DR vs MR}
				\begin{itemize}
					\item Rostrocaudal projection-contingent topographic organization \cite{Oegren2008,Vertes1999}
					\begin{itemize}
						\item DR projects more to lateral areas (e.g. Striatum, Amygdala)
						\item MR projects more to medial areas (e.g. Hippocampus)
					\end{itemize}
				\end{itemize}
				\begin{figure}[htp] \centering{
					\includegraphics[scale=0.35]{img/mrp.png}
					\hspace{1cm}
					\includegraphics[scale=0.35]{img/drp.png}}
					\caption{Schematic summary Median (\textbf{A}) and Dorsal Raphe (\textbf{B}) Nuclei projections in the rat \cite{Vertes1999}}
				\end{figure}
			\end{frame}
			\begin{frame}{Afferences}
				Many subcortical and some cortical regions \cite{Peyron1997}:
				\begin{itemize}
					\item Lateral habenula, medial and lateral preoptic areas, hypothalamic areas
					\item Orbital, Cingulate, Infralimbic and Insular Cortices
				\end{itemize}
				\begin{figure}[htp] \centering{
					\includegraphics[scale=0.25]{img/drna.jpg}}
					\caption{Excitatory and inhibitory afferences of the DR \cite{SoizaReilly2014}}
				\end{figure}
			\end{frame}
	\section{Ventral Tegmental Area (VTA)}
		\subsection{\textcolor{myorange}{Dopaminergic Forebrain Innervation}}
			\begin{frame}{DA Formations: 3-4 distinct pathways}
				\begin{itemize}
					\item Mesolimbic pathway \textcolor{lg}{($\subset$ Mesocorticolimbic projection)} - VTA
					\item Mesocortical pathway \textcolor{lg}{($\subset$ Mesocorticolimbic projection)} - VTA
					\item Nigrostriatal pathway - Substantia Nigra pars compacta (SNc)
					\item Tuberoinfundibular pathway - Arcuate Nucleus
				\end{itemize}
				\begin{figure}[htp] \centering{
					\includegraphics[scale=0.6]{img/dap.jpg}}
					\caption{Dopaminergic brainstem formations \cite{Money2013}}
				\end{figure}
			\end{frame}
			\begin{frame}{DA: Mesocorticolimbic vs. Nigrostriatal}
				\begin{figure}[htp] \centering{
					\includegraphics[scale=0.35]{img/dap1.jpg}}
					\caption{SNc and VTA projections and related networks \cite{M2013}}
				\end{figure}
			\end{frame}
			\begin{frame}{Appearance, Location, Structure}
				\begin{minipage}{.45\textwidth}
					The Ventral Tegmental Area:
					\begin{itemize}
						\item In the ventral midbrain, between:
						\begin{itemize}
							\item interpeduncular nucleus
							\item medial lemniscus
							\item cerebral peduncle
						\end{itemize}
					\end{itemize}
				\end{minipage}
				\begin{minipage}{.51\textwidth}
				\vspace{-2em}
				\py{pytex_fig(
					'scripts/vta.py',
					conf='slides/map_nocbar.conf',
					caption='
						Volumetric plot of the VTA.
						\\vspace{-1em}
						',
					label='dr',
					options_pre='\\centering\n',
					environment='figure',
					)}
				\end{minipage}\hfill
			\end{frame}
			\begin{frame}{Projections}
				\begin{itemize}
					\item Smaller extent, but better understood than DR projections.
				\end{itemize}
				\begin{figure}[htp] \centering{
					\includegraphics[scale=0.6]{img/vta.pdf}}
					\caption{Regions from \cite{Aransay2015,Fields2007,Ikemoto2007,Hnasko2012,Pan2010}}
				\end{figure}
			\end{frame}
			\begin{frame}{Highly Stable Functional Effects}
				\begin{minipage}{.45\textwidth}
					\begin{figure}[htp] \centering{
						\includegraphics[width=.95\textwidth]{img/vta_f_lohani.png}}
						\caption{From \cite{Lohani2016}}
					\end{figure}
				\end{minipage}
				\begin{minipage}{.45\textwidth}
					\vspace{-1em}
					\py{pytex_fig(
						'scripts/vta_f.py',
						conf='slides/map.conf',
						options_pre_caption='\\vspace{-2.5em}',
						caption='
							From \\cite{opfvta}
							',
						label='drf',
						options_pre='\\centering\n',
						environment='figure',
						)}
				\end{minipage}\hfill
			\end{frame}
			\begin{frame}{Afferences}
				\begin{itemize}
					\item Mediated by the VTA tail (VTAt) - a GABAergic inhibition hub \cite{Bourdy2012}
					\item Note afferences from DR
				\end{itemize}
				\begin{figure}[htp] \centering{
					\includegraphics[scale=0.7]{img/vtaa.jpg}}
					\caption{Schematic Overview of VTAt afferences and efferences \cite{Bourdy2012}}
				\end{figure}
			\end{frame}

	\section{Locus Caeruleus (LC)}
		\subsection{\textcolor{cerulean}{Dopaminergic Forebrain Innervation}}
			\begin{frame}{A Bit of Etymology}
				\begin{itemize}
					\item From “caeruleus” $=$ “sky-blue” [Latin]
					\item Many variant spellings, all at some point recommended by \textit{Nomina Anatomica}:
					\begin{itemize}
						\item \textcolor{lg}{1895:} Locus C\textcolor{cerulean_dark}{\textbf{ae}}ruleus
						\item \textcolor{lg}{1955:} Locus C\textcolor{cerulean_dark}{\textbf{oe}}ruleus
						\item \textcolor{lg}{1961:} Locus C\textcolor{cerulean_dark}{\textbf{e}}ruleus
						\item \textcolor{lg}{1983:} Locus C\textcolor{cerulean_dark}{\textbf{oe}}ruleus
						\item \textcolor{lg}{1989:} Locus C\textcolor{cerulean_dark}{\textbf{ae}}ruleus
					\end{itemize}
				\end{itemize}
			\end{frame}
			\begin{frame}{So, is it sky-blue?}
				No --- dark color from neuromelanin.
				\begin{figure}[htp]
					\centering
					\captionsetup{justification=centering}
					\includegraphics[scale=0.5]{img/lcr.png}
					\caption{Human brain slab depicting the Pons and the Locus Caeruleus - CC-BY-NC-SA Claudia Krebs - University of British Columbia \cite{M2013}}
				\end{figure}
			\end{frame}
			\begin{frame}{Projections and Structure}
				\begin{itemize}
					\item “the only” ascending noradrenergic pathway
					\item $\approx$ 1500 neurons in the rat
					\item Extensive branching - same neuron can project to cerebellum, brainstem, forebrain
					\item Projects to all major forebrain regions except basal ganglia, but including the VTA \cite{Isingrini2016}.
				\end{itemize}
				\begin{figure}[htp] \centering{
					\includegraphics[scale=0.7]{img/lcp.jpg}}
					\caption{Schematic of the LC's extensive projections \cite{Sara2009}}
				\end{figure}
			\end{frame}
			\begin{frame}{Afferences}
				\begin{itemize}
					\item Predominantly excitatory
					\item Note afferences from DR
				\end{itemize}
				\begin{figure}[htp] \centering{
					\includegraphics[scale=1.2]{img/lca.jpg}}
					\caption{Schematic Overview of LC afferences and efferences \cite{Delaville2011}}
				\end{figure}
			\end{frame}

	\section{Summary}
		\subsection{Meaningful generalities}
			\begin{frame}{Rankings}
				\begin{itemize}
					\item \textcolor{lg}{How well do we understand the network connections?} VTA\textgreater  LC\textgreater  DR
					\item \textcolor{lg}{How extensive are the projections?} LC\textgreater DR\textgreater  VTA
				\end{itemize}
			\end{frame}
			\begin{frame}{Main Gaps in Understanding}
				\begin{itemize}
					\item There is no standardized summary of structural or functional connectivity.
					\item Understanding of VTA-DR-LC interconnectivity is particularly poor.
					\item Small nuclei may also be unsuited for viral tracing (e.g. Allen Brain Connectivity Atlas).
					\begin{figure}[htp] \centering{
						\includegraphics[scale=0.23]{img/problems_lc.png}}
					\end{figure}
				\end{itemize}
			\end{frame}
	\section{References}
		\bibliographystyle{unsrt}
		\bibliography{./bib}
\end{document}
