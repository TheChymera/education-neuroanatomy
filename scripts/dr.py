import samri.plotting.maps as maps
from samri.fetch.local import roi_from_atlaslabel

stat_map = "/usr/share/mouse-brain-atlases/dsurqec_200micron_roi-dr.nii"
template = "/usr/share/mouse-brain-atlases/dsurqec_40micron_masked.nii"

maps.stat3D(stat_map,
	scale=0.3,
	alpha=0.9,
	template=template,
	show_plot=False,
	threshold=0.8,
	threshold_mesh=0.8,
	draw_colorbar=False,
	cmap=['#61aa3e'],
	)
